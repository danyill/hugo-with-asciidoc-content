---
title: "Demo Post"
date: 2018-12-10T17:21:29Z
---

This is a demo of a Markdown file being processed by Hugo to become part of the site in the 'content' folder.

Link to demo 'book' file in [HTML](/docs/results-user-manual.html) and [PDF](/docs/results-user-manual.pdf) format.