#!/bin/bash

for filename in $(find docs -maxdepth 3 -name '*.adoc'); do asciidoctor -r asciidoctor-html5s -b html5s --destination-dir=static/docs $filename; done
for filename in $(find docs -maxdepth 3 -name '*.adoc'); do asciidoctor-pdf -r asciidoctor-mathematical --destination-dir=static/docs $filename; done

for filename in $(find content -name '*.adoc');
  do 
    asciidoctor-pdf -a skip-front-matter -r asciidoctor-mathematical -o $(dirname "$filename")/$(basename $(dirname "$filename")).pdf $filename;
  done