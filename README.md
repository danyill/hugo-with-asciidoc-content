# Hugo with Asciidoc Content

Setup to demo Hugo site with mixed Markdown and Asciidoc files in /content.

## Update Docker Image and Push to Repo

* `docker build -t hugo-asciidoctor .`
* `docker tag hugo-asciidoctor bwklein/hugo-asciidoctor`
* `docker push bwklein/hugo-asciidoctor`

## Run Docker Image Locally

This will run the local image and mount the current directory to `/documents/` in the container.

`docker run -it -v ${pwd}:/documents/ bwklein/hugo-asciidoctor`

## Generate all Asciidoc HTML and PDF files.

`./scripts/build-docs.sh`

## Run Hugo to create the website.

`hugo`

## NOTES:

http://ratfactor.com/hugo-adoc-html5s/